package com.example.workflow.function;

import com.atlassian.jira.plugin.workflow.AbstractWorkflowPluginFactory;
import com.atlassian.jira.plugin.workflow.WorkflowPluginFunctionFactory;
import com.atlassian.jira.workflow.JiraWorkflow;
import com.atlassian.jira.workflow.WorkflowManager;
import com.opensymphony.workflow.loader.AbstractDescriptor;
import com.opensymphony.workflow.loader.FunctionDescriptor;
import java.util.HashMap;
import java.util.Map;
import webwork.action.ActionContext;

public class MyPostFunctionFactory extends AbstractWorkflowPluginFactory
        implements WorkflowPluginFunctionFactory
{
    public static final String FIELD_MESSAGE = "messageField";
    private WorkflowManager workflowManager;

    public MyPostFunctionFactory(WorkflowManager workflowManager)
    {
        this.workflowManager = workflowManager;
    }

    protected void getVelocityParamsForInput(Map<String, Object> velocityParams)
    {
        Map myParams = ActionContext.getParameters();

        JiraWorkflow jiraWorkflow = this.workflowManager.getWorkflow(((String[])myParams.get("workflowName"))[0]);

        velocityParams.put("messageField", "Workflow Last Edited By " + jiraWorkflow.getUpdateAuthorName());
    }

    protected void getVelocityParamsForEdit(Map<String, Object> velocityParams, AbstractDescriptor descriptor)
    {
        getVelocityParamsForInput(velocityParams);
        getVelocityParamsForView(velocityParams, descriptor);
    }

    protected void getVelocityParamsForView(Map<String, Object> velocityParams, AbstractDescriptor descriptor)
    {
        if (!(descriptor instanceof FunctionDescriptor))
        {
            throw new IllegalArgumentException("Descriptor must be a FunctionDescriptor.");
        }

        FunctionDescriptor functionDescriptor = (FunctionDescriptor)descriptor;

        String message = (String)functionDescriptor.getArgs().get("messageField");

        if (message == null) {
            message = "No Message";
        }

        velocityParams.put("messageField", message);
    }

    public Map<String, ?> getDescriptorParams(Map<String, Object> formParams)
    {
        Map params = new HashMap();

        String message = extractSingleParam(formParams, "messageField");
        params.put("messageField", message);

        return params;
    }
}
