package ut.com.atlassian.jira.plugins.workflow.sharing.pageobjects;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.query.TimedCondition;

public class WorkflowImported extends WizardPage<Summary, WorkflowImported>
{
    @ElementBy(id = "finishButton")
    private PageElement finishButton;

    @ElementBy(className = "wfshare-notes-warning")
    private PageElement additionalConfigurationWarning;

    @ElementBy(cssSelector = ".wfshare-notes-container textarea")
    private PageElement notesContainer;

    @Override
    public TimedCondition isAt()
    {
        return finishButton.timed().isVisible();
    }

    @Override
    public String getUrl()
    {
        return "/plugins/servlet/wfshare-import/import-workflow-success";
    }

    @Override
    protected Class<WorkflowImported> getNextClass()
    {
        return WorkflowImported.class;
    }

    @Override
    protected Class<Summary> getBackCass()
    {
        return Summary.class;
    }

    public void finish()
    {
        finishButton.click();
    }

    public boolean isAdditionalConfigurationWarningShown()
    {
        return additionalConfigurationWarning.isVisible();
    }

    public String getNotes()
    {
        return notesContainer.getValue();
    }
}
