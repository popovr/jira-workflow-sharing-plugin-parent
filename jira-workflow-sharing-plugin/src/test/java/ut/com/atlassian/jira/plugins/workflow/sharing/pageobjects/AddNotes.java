package ut.com.atlassian.jira.plugins.workflow.sharing.pageobjects;

import com.atlassian.pageobjects.elements.ElementBy;
import com.atlassian.pageobjects.elements.PageElement;

public class AddNotes extends WizardPage<StartExport, ExportSuccess>
{
    @ElementBy(id = "notes")
    private PageElement notes;

    @Override
    public String getUrl()
    {
        return "/plugins/servlet/wfshare-export/add-notes";
    }

    @Override
    protected Class<ExportSuccess> getNextClass()
    {
        return ExportSuccess.class;
    }

    @Override
    protected Class<StartExport> getBackCass()
    {
        return StartExport.class;
    }

    public String getNotes()
    {
        return notes.getValue();
    }
}
