## SECTION TITLES ##
wfshare.section.title.import=Import Workflow
wfshare.section.title.select=Select a Workflow
wfshare.section.title.export=Export Workflow
wfshare.section.title.list=Import/Export Workflows

## TAB NAMES ##
wfshare.import.tab.name=Workflow Import
wfshare.export.tab.name=Workflow Export

## EXPORT WIZARD TABS ##
wfshare.export.wizard.tab.verify-plugins=Plugins
wfshare.export.wizard.tab.notes=Notes
wfshare.export.wizard.tab.do-export=Export

## COMMON LABELS ##
wfshare.admin.link.label=Workflow Import/Export
wfshare.button.label.next=Next
wfshare.button.label.back=Back
wfshare.button.label.cancel=Cancel
wfshare.button.label.return=Return to Workflows
wfshare.button.label.done=Done
wfshare.label.workflow.name=Workflow Name:
wfshare.screen.plugin.label.key=Plugin key:
wfshare.screen.plugin.label.developer.url=Developer URL:
wfshare.screen.plugin.label.version=Plugin version:
wfshare.learnmore=Learn more

## JIRA List Workflows Screen ##
wfshare.screen.workflows.label.export=Export
wfshare.screen.workflows.label.import=Import a workflow
wfshare.error.screen.error=Error

## JIRA Workflow Screen ##
wfshare.screen.workflow.xml=XML
wfshare.screen.workflow.workflow.bundle=Workflow

#### IMPORT SCREENS ####
wfshare.import.screen.label.original.status=Original Status
wfshare.import.screen.label.new.status=New Status

## Choose Bundle Screen ##
wfshare.import.screen.bundle.title=Import a Workflow
wfshare.import.screen.bundle.instructions=Please select the workflow you would like to import into JIRA
wfshare.import.screen.bundle.from-computer=From My Computer
wfshare.import.screen.bundle.from-marketplace=From Atlassian Marketplace
wfshare.import.screen.bundle.from-computer.file=Workflow file
wfshare.import.screen.bundle.loading.plugin.details=Loading workflow details...
wfshare.import.screen.bundle.out.of.stars=out of 4 stars
wfshare.import.screen.bundle.download=Download
wfshare.import.screen.bundle.download.plural=Downloads
wfshare.import.screen.bundle.screenshots=Screenshots
wfshare.import.screen.bundle.version=Version
wfshare.import.screen.bundle.support=Support and Issues
wfshare.import.screen.bundle.reviews=Customer Reviews
wfshare.import.screen.bundle.view.marketplace=View on Marketplace
wfshare.import.screen.bundle.loading.bundles=Loading workflows...
wfshare.import.screen.bundle.loading.details=Loading workflow details...
wfshare.import.screen.bundle.see.more=See more workflows
wfshare.import.screen.bundle.loading.error=Unable to reach {0}Atlassian Marketplace{1} to browse workflows.
wfshare.import.screen.bundle.downloading.bundle=Downloading...

## Choose Name Screen ##
wfshare.import.screen.name.title=Choose a Workflow Name
wfshare.import.screen.name.instructions=Workflow name
wfshare.import.screen.name.error.exists=Workflow name already exists

## Map Status Screen ##
wfshare.import.screen.status.title=Map Workflow Statuses
wfshare.import.screen.status.instructions=Map statuses in the workflow to new or existing statuses in JIRA.
wfshare.import.screen.status.select.option.create-status=New Status
wfshare.import.copy.prefix=\ - {0}
wfshare.import.copy={0}{1}

## Summary Screen ##
wfshare.import.screen.summary.title=Preview of import
wfshare.import.screen.summary.plugin.warning=The workflow that you are importing contains a number of plugins. The workflow importer will not install these plugins or any of the workflow functionality that depends on each plugin.
wfshare.import.screen.summary.disabled.fields.warning.sysadmin=The workflow that you are importing contains custom fields which are disabled in this instance of JIRA. The workflow importer will not create these fields unless they are enabled before importing. You can enable custom fields by enabling the corresponding modules of the {0}{1}{2} plugin. You can refresh this summary page after enabling the custom fields to see the current state.
wfshare.import.screen.summary.disabled.fields.warning.admin=The workflow that you are importing contains custom fields which are disabled in this instance of JIRA. The workflow importer will not create these fields.
wfshare.import.screen.summary.label.statuses=Statuses
wfshare.import.screen.summary.label.status.mappings.description=The following statuses will be created in your instance of JIRA.
wfshare.import.screen.summary.label.custom.fields=Custom Fields
wfshare.import.screen.summary.label.custom.fields.description=The following custom fields will be created in your instance of JIRA.
wfshare.import.screen.summary.label.screens=Screens
wfshare.import.screen.summary.label.screens.description=The following screens will be created in your instance of JIRA.
wfshare.import.screen.summary.import=Import
wfshare.import.screen.summary.status=Status
wfshare.import.screen.summary.no.new=No new statuses, screens or custom fields will be created for this workflow.

## Success Screen ##
wfshare.import.screen.success.title=Workflow Imported
wfshare.import.screen.success.message.title=Workflow has been successfully imported
wfshare.import.screen.success.additional.configuration=Additional Configuration
wfshare.import.screen.success.additional.instructions=When this workflow was exported, additional configuration notes were provided by the author. These notes may provide information on how you can configure the imported workflow to its original state.
wfshare.import.screen.success.additional.configuration.warning=Please copy these notes for your records.

##Import errors##
wfshare.import.error.generic=Unable to import workflow.
#### EXPORT SCREENS ####

## Plugin Screen ##
wfshare.export.screen.export.title=Export Your Workflow
wfshare.export.screen.export.description=Where you can use your workflow
wfshare.export.screen.export.exporting.to.jira=With another JIRA Instance
wfshare.export.screen.export.exporting.to.jira.description=Import this workflow on another instance of JIRA.
wfshare.export.screen.export.exporting.to.marketplace=On the Atlassian Marketplace
wfshare.export.screen.export.exporting.to.marketplace.description=Share your workflows with other users on the Atlassian Markeplace.

## Notes Screen ##
wfshare.export.screen.notes.title=Add Notes
wfshare.export.screen.notes.instructions=Special instructions to include
wfshare.export.screen.notes.instructions.description=These instructions will be displayed when importing the workflow and should contain things like details about custom fields and screen setup.
wfshare.export.notes.removed.message=The items from the following plugins were removed from the workflow:
wfshare.export.notes.transition=Transition: {0}
wfshare.export.notes.plugin.conditions=Plugin conditions removed from this transition:
wfshare.export.notes.system.conditions=System conditions removed from this transition:
wfshare.export.notes.plugin.validators=Plugin validators removed from this transition:
wfshare.export.notes.system.validators=System validators removed from this transition:
wfshare.export.notes.plugin.functions=Plugin post functions removed from this transition:
wfshare.export.notes.system.functions=System post functions removed from this transition:
wfshare.export.notes.plugin.custom.fields=Plugin custom fields removed from this transition:
wfshare.export.notes.custom.event.replaced=Custom event fired by Fire Event Function was replaced by the generic event.
wfshare.export.screen.notes.export=Export

## Success Screen ##
wfshare.export.screen.success.title=Workflow Exported
wfshare.export.screen.success.message.title=The workflow has been successfully exported
wfshare.export.screen.success.message=If your download does not start automatically, click {0}here{1} to restart the download. Note: This file will be removed in {2} minutes.
wfshare.export.screen.success.back=Back to workflow
wfshare.export.screen.success.whatsnext=What's next?
wfshare.export.screen.success.sharemarketplace=Share with the Atlassian Marketplace
wfshare.export.screen.success.sharemarketplace.description=You can share your workflows with other users by uploading them to the Atlassian Marketplace.
wfshare.export.screen.success.sharemarketplace.link=Visit the Atlassian Marketplace
wfshare.export.screen.success.importjira=Import on another JIRA Instance
wfshare.export.screen.success.importjira.description=You can import this workflow on another instance of JIRA.

wfshare.export.file.not.found=The exported file was not found. Note that old exported workflows are deleted automatically after {0} minutes.
wfshare.export.could.not.read.file=Could not read the exported file.
wfshare.export.could.not.write.file=Could not create the exported file

## Errors ##
wfshare.export.error.generic=Unable to export workflow.

## Java Messaging ##
wfshare.import.prefix=Import {0} of {1}

## Java Exceptions ##
wfshare.exception.workflow.xml.not.found.in.zip=File does not contain a workflow.
wfshare.exception.workflow.too.big=File is too large to import.
wfshare.exception.uploading.by.field.not.supported=uploading by field not supported!
wfshare.exception.file.must.be.a.zip.file=File is invalid.
wfshare.exception.unknown.file=File is invalid: {0}!
wfshare.exception.invalid.file=File is invalid: {0}!
wfshare.exception.import.data.not.found.in.session=Import wizard must be restarted.
wfshare.exception.export.data.not.found.in.session=Export wizard must be restarted.
wfshare.exception.import.invalid.id=Invalid project or workflow scheme ID.
wfshare.exception.status.holders.not.found.in.session=Status Holders not found in Session.
wfshare.exception.unknown.status.id=unknown workflow xml status id: {0}
wfshare.exception.no.workflow.name.param=Workflow NAME was not passed as a parameter!
wfshare.exception.workflow.not.found=Could not find workflow with name: {0}
wfshare.exception.null.workflow.zip=Could not create zip file for workflow: {0}. Please check the JIRA logs.
wfshare.exception.download.error=Error downloading file from Atlassian Marketplace. HTTP Status code: {0}
wfshare.exception.cannot.include.v1.plugin=This workflow contains a V1 plugin which cannot be exported! Plugin Name: {0}
wfshare.exception.error.parsing.version=Error parsing version\: {0}
wfshare.exception.not.a.multipart.form.request=Not a Multipart form request\!
wfshare.exception.workflow.xml.is.blank=Workflow XML is blank\!
wfshare.exception.unable.to.create.workflow.descriptor=Unable to create workflow descriptor, import cancelled.
wfshare.exception.unable.to.migrate.statuses=Unable to migrate statuses, import cancelled.
wfshare.exception.unable.to.read.custom.field.data=Unable to read custom field data, import cancelled.
wfshare.exception.unable.to.migrate.custom.fields=Unable to migrate custom fields, import cancelled.
wfshare.exception.unable.to.read.screen.data=Unable to read screen data, import cancelled.
wfshare.exception.unable.to.migrate.screens=Unable to migrate screens, import cancelled.
wfshare.exception.unable.to.create.workflow=Unable to create workflow, import cancelled.
wfshare.exception.module.classname.should.not.be.null=Module classname should not be null when constructing ModuleOfClassnamePredicate\!
wfshare.exception.status.not.found=status not found\!
wfshare.error.not.sys.admin=This operation is only permitted to system administrators.
