#* @vtlvariable name="plugins" type="java.util.List<java.lang.String>" *#
#* @vtlvariable name="newStatuses" type="java.util.List<com.atlassian.jira.issue.status.SimpleStatus>" *#
#* @vtlvariable name="screens" type="java.util.List<com.atlassian.jira.plugins.workflow.sharing.model.ScreenInfo>" *#
#* @vtlvariable name="enabledFields" type="java.util.List<com.atlassian.jira.plugins.workflow.sharing.importer.servlet.ImportWizardHandler.CustomFieldBean>" *#
#* @vtlvariable name="disabledFields" type="java.util.List<com.atlassian.jira.plugins.workflow.sharing.importer.servlet.ImportWizardHandler.CustomFieldBean>" *#
#* @vtlvariable name="workflowName" type="java.lang.String" *#


$webResourceManager.requireResourcesForContext("jira.workflow.sharing")
<html>
<head>
    <title>$i18n.getText("wfshare.section.title.import")</title>
    <meta name="decorator" content="panel-admin"/>
</head>
<body class="aui-page-focused aui-page-focused-large">
    <h2>$i18n.getText("wfshare.import.screen.summary.title")</h2>

    #if(!$plugins.empty)
        <div id="plugin-warning" class="aui-message warning">
            <div class="title">
                <span class="aui-icon icon-warning"></span>
                <strong>$i18n.getText("wfshare.import.screen.summary.plugin.warning")</strong>
            </div>
            <ul>
                #foreach($plugin in $plugins)
                    <li class="plugin-name">$plugin</li>
                #end
            </ul>
        </div>
    #end

    #if(!$disabledFields.empty)
        <div id="custom-field-warning" class="aui-message warning">
            <div class="title">
                <span class="aui-icon icon-warning"></span>
                <strong>
                    #if($isSysAdmin)
                        #set($customFieldPluginName = $i18n.getText("admin.customfield.plugin.name"))
                        #set($customFieldPluginHref = "$requestContext.baseUrl/plugins/servlet/upm#manage/com.atlassian.jira.plugin.system.customfieldtypes")

                        #set($customFieldsWarningHtml = $i18n.getText("wfshare.import.screen.summary.disabled.fields.warning.sysadmin", "<a href='$customFieldPluginHref' target='_blank'>", $customFieldPluginName, "</a>"))
                        $customFieldsWarningHtml
                    #else
                        $i18n.getText("wfshare.import.screen.summary.disabled.fields.warning.admin")
                    #end
                </strong>
            </div>
            <ul>
                #foreach($disabledField in $disabledFields)
                    <li class="type-name" data-customfield-key="$disabledField.key">$disabledField.type</li>
                #end
            </ul>
        </div>
    #end

    <div class="wf-summary-container">
        #if(!$newStatuses.empty)
            <h4>$i18n.getText("wfshare.import.screen.summary.label.statuses")</h4>
            <p>$i18n.getText("wfshare.import.screen.summary.label.status.mappings.description")</p>
            <ul id="summary-statuses">
                #foreach($simpleStatus in $newStatuses)
                    <li class="summary-name">
                        #if ($statusLozengesEnabled)
                            #set ($isIssueStatusSubtle = true)
                            #set ($issueStatusMaxWidth = "long")
                            #parse("/static/util/issue/status.vm")
                        #else
                            $simpleStatus.name
                        #end
                    </li>
                #end
            </ul>
        #end

        #if(!$screens.empty)
            <h4>$i18n.getText("wfshare.import.screen.summary.label.screens")</h4>
            <p>$i18n.getText("wfshare.import.screen.summary.label.screens.description")</p>
            <table id="summary-screens" class="aui">
                <thead>
                    <tr>
                        <th class="screen-name">$i18n.getText("common.words.name")</th>
                        <th class="screen-description">$i18n.getText("common.words.description")</th>
                    </tr>
                </thead>
                <tbody>
                    #foreach($screen in $screens)
                        <tr>
                            <td class="summary-name">$screen.name</td>
                            <td>$!screen.description</td>
                        </tr>
                    #end
                </tbody>
            </table>
        #end

        #if(!$enabledFields.empty)
            <h4>$i18n.getText("wfshare.import.screen.summary.label.custom.fields")</h4>
            <p>$i18n.getText("wfshare.import.screen.summary.label.custom.fields.description")</p>
            <table id="summary-custom-fields" class="aui">
                <thead>
                    <tr>
                        <th class="custom-field-name">$i18n.getText("common.words.name")</th>
                        <th class="custom-field-type">$i18n.getText("admin.common.words.type")</th>
                        <th class="custom-field-description">$i18n.getText("common.words.description")</th>
                    </tr>
                </thead>
                <tbody>
                    #foreach($customField in $enabledFields)
                        <tr data-customfield-key="$!customField.key">
                            <td class="summary-name">$customField.name</td>
                            <td>$!customField.type</td>
                            <td>$!customField.description</td>
                        </tr>
                    #end
                </tbody>
            </table>
        #end
    </div>

    <form class="aui top-label dont-default-focus" action="$nextUrl" id="wfShareImportChooseName" method="post">
        <div class="buttons-container">
            <a href="$backUrl" class="aui-button" id="backButton">$i18n.getText("wfshare.button.label.back")</a>
            <input class="aui-button aui-button-primary" id="nextButton" type="submit" value="$i18n.getText("wfshare.import.screen.summary.import")"/>
            <a class="cancel" href="$cancelUrl">$i18n.getText("wfshare.button.label.cancel")</a>
        </div>
        <input name="atl_token" type="hidden" value="$atl_token" />
    </form>

    <script>
        JIRA.WSP.Analytics.sendEvent("import.step", {
            stepName: "summary"
        });
    </script>
</body>
</html>
