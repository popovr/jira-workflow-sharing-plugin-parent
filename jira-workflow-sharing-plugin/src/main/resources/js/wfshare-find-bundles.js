(function ($) {

    var localWFShareBundles = function () {

        var that = this;

        // Handle a click on a plugin row
        this.loadPlugins = function (page, projectId, schemeId) {
            AJS.$.ajax({
                url: AJS.contextPath() + "/rest/wfshare/1.0/workflowbundles/summary/" + page,
                type: 'get',
                cache: false,
                timeout: 60000,
                success: function (pluginsJson) {

                    var plugins = pluginsJson.plugins;

                    for (var i = 0, len = plugins.length; i < len; i++) {
                        var plugin = plugins[i];
                        var target;

                        plugin.nextFormUrl = $("#wfshareNextFormUrl").val();

                        var pluginEntry = $(JIRA.Templates.WFShare.pluginEntry({
                            plugin: plugin,
                            projectId: projectId,
                            schemeId: schemeId
                        }));

                        $("#workflowBundleListContainer").append(pluginEntry).removeClass('hidden');

                        target = pluginEntry.find('.workflow-preview');

                        $('.workflow-sharing-loading-container').hide();

                        if (wfshareBundles.importing === true) {
                            pluginEntry.find(".bundle-import-button").prop('disabled', true);
                        }
                        AJS.$.ajax({
                            url: AJS.contextPath() + "/rest/wfshare/1.0/workflowbundles/details/" + plugin.pluginKey,
                            type: 'get',
                            cache: false,
                            timeout: 60000,
                            success: (function(target) {
                                return function (data) {
                                    var img = data.media && data.media.screenshots && data.media.screenshots[0] && data.media.screenshots[0].image;
                                    if (img) {
                                        var link = AJS.$('<a href="' + img.links[1].href + '" class="workflow-image"></a>');
                                        var image = AJS.$('<img class="" src="' + img.links[1].href + '"/>').hide().load(function() { AJS.$(this).fadeIn(); });
                                        link.append(image).prependTo(target).fancybox();
                                    }
                                }
                            }(target))
                        });
                    }

                    var $seeMore = $('.workflow-sharing-see-more');
                    if (pluginsJson.nextOffset > -1) {
                        $seeMore.removeClass("hidden");
                        $seeMore.unbind('click').click(function (e) {
                            e.preventDefault();

                            if (!$seeMore.hasClass("disabled")) {
                                $seeMore.addClass("hidden");
                                $('.workflow-sharing-loading-container').show();
                                wfshareBundles.loadPlugins(pluginsJson.nextOffset, projectId, schemeId);
                            }
                        });

                        if (wfshareBundles.importing === true) {
                            $seeMore.prop("disabled", true).addClass("disabled");
                        }
                    } else {
                        $seeMore.addClass("hidden");
                    }
                    
                },
                error: function () {
                    $('.workflow-sharing-loading-container').hide();

                    that.showCouldNotContactMpacWarning("#workflow-sharing-message-bar", false);
                }
            });
            
        };

        this.showCouldNotContactMpacWarning = function (target, closeable) {
            AJS.messages.warning(target, {
                body: AJS.I18n.getText("wfshare.import.screen.bundle.loading.error", '<a href="https://marketplace.atlassian.com/">', '</a>'),
                closeable: closeable
            });
        };

        this.toggleMode = function(e) {
            var uploadContainer = $("#upload-container");
            var upmContainer = $("#workflow-sharing-container");
            var toShow = $(this).attr('href');
            var buttons = $('.workflow-source');

            e.preventDefault();

            // hide both
            uploadContainer.addClass("hidden");
            upmContainer.addClass("hidden");

            $(toShow).removeClass("hidden");
            $(this).attr('aria-pressed','true');
            buttons.not(this).removeAttr('aria-pressed');
        };

        this.downloadBundle = function(e) {
            e.preventDefault();

            wfshareBundles.importing = true;

            var importForm = $(this).closest("form");

            $(".bundle-import-button").prop('disabled', true);
            $(".workflow-sharing-see-more").prop('disabled', true).addClass("disabled");

            var indicator = importForm.find(".workflow-downloading-indicator");
            indicator.removeClass("hidden");

            var onRequestError = function (error, technical) {
                $(".bundle-import-button").prop('disabled', false);
                $(".workflow-sharing-see-more").prop('disabled', false).removeClass("disabled");
                indicator.addClass("hidden");

                var messageBar = importForm.parents(".workflow-entry");

                if (technical === true) {
                    that.showCouldNotContactMpacWarning(messageBar, true);
                } else {
                    AJS.messages.error(messageBar, {
                        body: error,
                        closeable: true
                    });
                }

                wfshareBundles.importing = false;
            };

            AJS.$.ajax({
                url: importForm.attr("action"),
                data: importForm.serialize(),
                type: 'post',
                cache: false,
                timeout: 60000,
                success: function (response) {
                    if (response && response.errorMessage) {
                        onRequestError(response.errorMessage, response.technical);
                    } else {
                        window.location.href = AJS.contextPath() + "/plugins/servlet/wfshare-import/set-workflow-name";
                    }
                },
                error: function (jqXHR, textStatus) {
                    onRequestError(textStatus, true);
                }
            });
        };
    };

    window.wfshareBundles = new localWFShareBundles();

})(AJS.$);

AJS.$(function ($) {
    // downloading the workflow
    $('.workflow-sharing-list').on('click', '.bundle-import-button', wfshareBundles.downloadBundle);

    // toggling the view (BTF only)
    $(".workflow-source").click(wfshareBundles.toggleMode);

    var projectId = $("#projectId").val();
    var schemeId = $("#schemeId").val();

    if ($('.workflow-sharing-list-container').length) {
        $('.workflow-sharing-loading-container').show();
        wfshareBundles.loadPlugins(0, projectId, schemeId);
    }

    $("#wfShareImportFile").change(function () {
        $("#nextButton").prop("disabled", !$(this).val());
    }).change();
});
