package com.atlassian.jira.plugins.workflow.sharing.event;

import com.atlassian.analytics.api.annotations.Analytics;

@Analytics ("importworkflow.viewed")
public class ImportWorkflowViewedEvent
{
    private final String src;

    public ImportWorkflowViewedEvent(String src)
    {
        this.src = src;
    }

    public String getSrc()
    {
        return src;
    }
}
