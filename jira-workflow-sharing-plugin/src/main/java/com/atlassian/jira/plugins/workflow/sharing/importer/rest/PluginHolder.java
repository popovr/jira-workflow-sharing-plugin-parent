package com.atlassian.jira.plugins.workflow.sharing.importer.rest;

import com.atlassian.marketplace.client.model.PluginSummary;
import org.codehaus.jackson.annotate.JsonCreator;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.List;

public class PluginHolder
{
    @JsonProperty("plugins") private List<PluginSummary> plugins;
    @JsonProperty("nextOffset") private Integer nextOffset;

    @JsonCreator
    public PluginHolder(@JsonProperty("plugins") List<PluginSummary> plugins, @JsonProperty("nextOffset") Integer nextOffset)
    {
        this.plugins = plugins;
        this.nextOffset = nextOffset;
    }

    public List<PluginSummary> getPlugins()
    {
        return plugins;
    }

    public void setPlugins(List<PluginSummary> plugins)
    {
        this.plugins = plugins;
    }

    public Integer getNextOffset()
    {
        return nextOffset;
    }

    public void setNextOffset(Integer nextOffset)
    {
        this.nextOffset = nextOffset;
    }
}
