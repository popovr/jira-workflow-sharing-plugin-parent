package com.atlassian.jira.plugins.workflow.sharing;

public interface WorkflowPluginHelper
{
    boolean isFromPlugin(String className);
}
