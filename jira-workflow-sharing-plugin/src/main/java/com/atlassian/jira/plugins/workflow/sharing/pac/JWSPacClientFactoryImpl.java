package com.atlassian.jira.plugins.workflow.sharing.pac;

import com.atlassian.jira.plugins.workflow.sharing.util.CommonsHttpClientHelper;
import com.atlassian.marketplace.client.MarketplaceClient;
import com.atlassian.marketplace.client.impl.DefaultMarketplaceClient;
import com.atlassian.marketplace.client.impl.JsonEntityEncoding;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import org.springframework.stereotype.Component;

@ExportAsService
@Component
public class JWSPacClientFactoryImpl implements JWSPacClientFactory
{
    private final JWSPacClient jwsPacClient;
    
    public JWSPacClientFactoryImpl()
    {
        MarketplaceClient marketplaceClient = new DefaultMarketplaceClient(DefaultMarketplaceClient.DEFAULT_SERVER_URI,
                new CommonsHttpClientHelper(), new JsonEntityEncoding());

        this.jwsPacClient = new MPacClientImpl(marketplaceClient);
    }

    @Override
    public JWSPacClient getPacClient()
    {
        return jwsPacClient;
    }
}
