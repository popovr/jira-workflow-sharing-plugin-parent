package com.atlassian.jira.plugins.workflow.sharing.lifecycle;

import com.atlassian.jira.plugins.workflow.sharing.file.FileCleaningJobScheduler;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Basically, the main method for the plugin.
 *
 * The {@link #onStart()} method is called JIRA is called when the plugin is enabled and JIRA is started as per the
 * {@link LifecycleAware} contract.
 *
 * The {@link #destroy()} method is called as the plugin is being disabled. The could be when the plugin is uninstalled
 * or disabled or when JIRA is shutting down (a plugin's Spring context is destroyed as the plugin is disabled).
 *
 * REF: https://developer.atlassian.com/x/mYGI
 */
@Component
@ExportAsService(LifecycleAware.class)
public class Launcher implements LifecycleAware, DisposableBean
{
    private final FileCleaningJobScheduler scheduler;

    @Autowired
    public Launcher(FileCleaningJobScheduler scheduler)
    {
        this.scheduler = scheduler;
    }

    @Override
    public void onStart()
    {
        scheduler.start();
    }

    @Override
    public void onStop()
    {
        scheduler.stop();
    }

    @Override
    public void destroy()
    {
        scheduler.stop();
    }
}
