package com.atlassian.jira.plugins.workflow.sharing.servlet;

import com.atlassian.jira.config.properties.ApplicationProperties;
import com.atlassian.jira.plugins.workflow.sharing.file.CanNotCreateFileException;
import com.atlassian.jira.security.xsrf.XsrfTokenGenerator;
import com.atlassian.jira.web.action.XsrfErrorAction;
import com.atlassian.sal.api.auth.LoginUriProvider;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.user.UserManager;
import com.atlassian.sal.api.xsrf.XsrfTokenValidator;
import com.opensymphony.workflow.FactoryException;
import org.apache.commons.lang.StringUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Map;

public abstract class AbstractServletWizardHandler extends AbstractServlet
{
    static final String JIRA_SERAPH_SECURITY_ORIGINAL_URL = "os_security_originalurl";
    static final String CONF_SERAPH_SECURITY_ORIGINAL_URL = "seraph_originalurl";
    protected static final String DATA_NOT_FOUND_IN_SESSION_PARAM = "notFoundInSession";

    private final LoginUriProvider loginUriProvider;
    private final UserManager userManager;
    protected final I18nResolver i18n;
    private final XsrfTokenValidator xsrfTokenValidator;
    private final XsrfTokenGenerator xsrfTokenGenerator;

    protected AbstractServletWizardHandler(LoginUriProvider loginUriProvider, UserManager userManager, I18nResolver i18n,
                                           XsrfTokenValidator xsrfTokenValidator, XsrfTokenGenerator xsrfTokenGenerator,
                                           ApplicationProperties applicationProperties)
    {
        super(applicationProperties);

        this.loginUriProvider = loginUriProvider;
        this.userManager = userManager;
        this.i18n = i18n;
        this.xsrfTokenValidator = xsrfTokenValidator;
        this.xsrfTokenGenerator = xsrfTokenGenerator;
    }

    protected interface RequestHandler
    {
        void handle(HttpServletRequest request, HttpServletResponse response,
                    Map<String, Object> context, ServletMapping requestMapping)
                throws DataNotFoundInSessionException, IOException, FactoryException, ValidationException,
                CanNotCreateFileException, URISyntaxException, ServletException;
    }

    protected boolean enforceAdminLogin(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        if (!isAdmin())
        {
            redirectToLogin(request,response);

            return true;
        }

        return false;
    }

    protected boolean isAdmin()
    {
        if (userManager.getRemoteUsername() == null)
        {
            return false;
        }
        return userManager.isSystemAdmin(userManager.getRemoteUsername()) || userManager.isAdmin(userManager.getRemoteUsername());
    }

    protected boolean isSystemAdmin()
    {
        if (userManager.getRemoteUsername() == null)
        {
            return false;
        }
        return userManager.isSystemAdmin(userManager.getRemoteUsername());
    }

    protected void redirectToLogin(HttpServletRequest request, HttpServletResponse response) throws IOException
    {
        final URI uri = getUri(request);
        addSessionAttributes(request, uri.toASCIIString());
        response.sendRedirect(loginUriProvider.getLoginUri(uri).toASCIIString());
    }

    protected URI getUri(HttpServletRequest request)
    {
        StringBuffer builder = request.getRequestURL();
        if (request.getQueryString() != null)
        {
            builder.append("?");
            builder.append(request.getQueryString());
        }
        return URI.create(builder.toString());
    }

    protected <T> T getSessionAttribute(HttpServletRequest request, String name, String errorMsg) throws ValidationException
    {
        T type;
        try
        {
            type = (T) request.getSession().getAttribute(name);
            if(null == type)
            {
                throw new ValidationException(errorMsg);
            }
        }
        catch (Exception e)
        {
            throw new ValidationException(errorMsg);
        }

        return type;
    }

    protected <T> T getSessionAttributeOrNull(HttpServletRequest request, String name)
    {
        return (T) request.getSession().getAttribute(name);
    }

    private void addSessionAttributes(final HttpServletRequest request, final String uriString)
    {
        request.getSession().setAttribute(JIRA_SERAPH_SECURITY_ORIGINAL_URL, uriString);
        request.getSession().setAttribute(CONF_SERAPH_SECURITY_ORIGINAL_URL, uriString);
    }

    protected void clearSessionAttributes(final HttpSession session)
    {
        session.removeAttribute(JIRA_SERAPH_SECURITY_ORIGINAL_URL);
        session.removeAttribute(CONF_SERAPH_SECURITY_ORIGINAL_URL);
    }

    protected String getMappingPath(HttpServletRequest request)
    {
        String lastPath = StringUtils.substringAfterLast(request.getRequestURI(), "/");
        if (StringUtils.isBlank(lastPath))
        {
            lastPath = StringUtils.substringBeforeLast(request.getRequestURI(), "/");
        }

        return lastPath;
    }

    protected void addXsrfToken(Map<String, Object> context, HttpServletRequest request)
    {
        String token = getXsrfToken(request);
        context.put("atl_token", token);
    }

    protected String getXsrfToken(HttpServletRequest request)
    {
        return xsrfTokenGenerator.generateToken(request);
    }

    protected boolean checkXsrf(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException
    {
        if (xsrfTokenValidator.validateFormEncodedToken(request))
        {
            return false;
        }

        request.getRequestDispatcher(XsrfErrorAction.FORWARD_PATH).include(request, response);

        return true;
    }
}
