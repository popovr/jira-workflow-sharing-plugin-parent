package com.atlassian.jira.plugins.workflow.sharing.file;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.sal.api.scheduling.PluginJob;
import com.atlassian.sal.api.scheduling.PluginScheduler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

@ExportAsService(FileCleaningJobScheduler.class)
@Component
public class FileCleaningJobSchedulerImpl implements FileCleaningJobScheduler
{
    private final Logger LOG = LoggerFactory.getLogger(FileCleaningJobSchedulerImpl.class);

    private static final String JOB_KEY = "com.atlassian.jira.plugins.workflow.sharing.file.FileCleaningJob";
    private static final String MAX_AGE_KEY = "MAX_AGE";
    private static final long DEFAULT_INTERVAL = TimeUnit.HOURS.toMillis(1);

    private final PluginScheduler pluginScheduler;

    //Protected by this.
    private boolean isScheduled = false;

    @Autowired
    public FileCleaningJobSchedulerImpl(final PluginScheduler pluginScheduler)
    {
        this.pluginScheduler = pluginScheduler;
    }

    @Override
    public synchronized void start()
    {
        LOG.debug("Adding schedule for FileCleaningJobScheduler.");

        if (!isScheduled)
        {
            schedule();
            isScheduled = true;
        }
    }

    @Override
    public synchronized void stop()
    {
        LOG.debug("Removing schedule for FileCleaningJobScheduler.");

        if (isScheduled)
        {
            unschedule();
            isScheduled = false;
        }
    }

    private void schedule()
    {
        LOG.debug("Scheduling deletion of old workflow files to " + DEFAULT_INTERVAL + " minutes");

        Map<String, Object> jobData = new HashMap<String, Object>();
        jobData.put(MAX_AGE_KEY, DEFAULT_INTERVAL);

        pluginScheduler.scheduleJob(JOB_KEY, FileCleaningJob.class, jobData, new Date(System.currentTimeMillis() + DEFAULT_INTERVAL), DEFAULT_INTERVAL);
    }

    private void unschedule()
    {
        pluginScheduler.unscheduleJob(JOB_KEY);
    }

    public long getCurrentMaxAge()
    {
        return DEFAULT_INTERVAL;
    }

    public static class FileCleaningJob implements PluginJob
    {
        private final Logger LOG = LoggerFactory.getLogger(FileCleaningJob.class);

        @Override
        public void execute(final Map<String, Object> jobDataMap)
        {
            try
            {
                Long maxAgeInMs = (Long) jobDataMap.get(MAX_AGE_KEY);
                FileManager fileManager =  ComponentAccessor.getOSGiComponentInstanceOfType(FileManager.class);

                fileManager.clearOlderThan(maxAgeInMs);
            }
            catch (Exception e)
            {
                LOG.error("Error occurred while cleaning workflow exports.", e);
            }
        }
    }
}
